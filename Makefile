DESTDIR ?= ""
EVDEV_RULES := /usr/share/X11/xkb/rules/evdev

X11_DESTDIR := $(DESTDIR)/usr/share/X11
XKB_DESTDIR := $(X11_DESTDIR)/xkb
SYMS_DIR := $(XKB_DESTDIR)/symbols
RULES_DIR := $(XKB_DESTDIR)/rules
CONF_DIR := $(X11_DESTDIR)/xorg.conf.d

all: rules/sucks

rules/sucks: rules/sucks.part $(EVDEV_RULES)
	cat $(EVDEV_RULES) $^ > $@

clean:
	rm -f rules/sucks

install:
	install -m 0755 -d $(SYMS_DIR) $(RULES_DIR) $(CONF_DIR)
	install -m 0644 -t $(SYMS_DIR) $(wildcard symbols/*)
	install -m 0644 -t $(CONF_DIR) $(wildcard xorg.conf.d/*.conf)
	install -m 0644 -t $(RULES_DIR) rules/sucks
